Random Hacks
============

This is a repository for ugly/interresting/fun pieces of code.

### vtable.cpp

Accessing methods from a NULL pointer. virtual vs not virtual.

### evil_shift.c

Fun with shifts and negative numbers.

### evil_switch.c

Stupid typo. Impossible to debug without syntaxic highliting.

### strlen.c

strlen in O(1) at compile time.

### ++i++.c

Proof that the syntax `++i++` can be valid in a C program.

### aldebaran.c

Can you guess what the output will be ?

### eval_order.c

Always remember that the evaluation order is sometime undefined!

### magick_floats.c

A tricky use of the `,` operator

### pointer_align.c

A trick to put 2 bits of data in a pointer.

### to_zero.c

Fun syntax for decrementing loops

/* This code demonstrate how to include some flags in a standard pointer */
/* It only works if pointer adress are multiple of 4 */

#include <stdio.h>

#define FLAG_A 1
#define FLAG_B 2

// Return a pointer containing the flags
void *set_flags(void *p, int flags)
{
  return (void*)((long long)p | (flags & 3));
}

// Retreive flags from pointer
int get_flags(void* p)
{
  return (long long)p & 3;
}

// Retreive adress from pointer
void *get_addr(void *p)
{
  return (void*)((long long)p & ~3);
}

int main()
{
  int a = 42;
  int *p = &a;

  void *x = set_flags(p, FLAG_A | FLAG_B);

  printf("data  = %d\n", *((int*)get_addr(x)));
  printf("flags = %d\n", get_flags(x));

  return 0;
}

#include <stdio.h>

int main()
{
  int x;

  x = 0;
  printf("%d - %d - %d\n", ++x, ++x, ++x); // 3 - 3 - 3

  x = 0;
  printf("%d - %d - %d\n", x++, x++, x++); // 2 - 1 - 0

  return 0;
}

#include <stdio.h>

// strlen in O(1) at compile time
#define MY_STRLEN(S) (~(S - " "))

int main(void)
{
  printf("%d\n", MY_STRLEN("123456789abc"));

  return 0;
}

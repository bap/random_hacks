#include <iostream>

struct A
{
  void lol()
  {
    std::cout << "lol\n";    
  }

  virtual void lol2()
  {
    std::cout << "lol2\n";    
  }
};

int main(void)
{
  std::cout << "call lol()\n";
  ((A*)NULL)->lol();
  std::cout << "call lol2()\n";
  ((A*)NULL)->lol2();
  return 0;
}
